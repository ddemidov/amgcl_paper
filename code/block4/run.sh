#!/bin/bash

#!/bin/bash

export OCL_MAX_DEVICES=1
rm *.txt

# --- GPU runs -----------------------------------------------------------
for i in `seq 5`; do
    echo -----------------------------------------------------------------
    echo --- CORES=$n, i=$i
    echo -----------------------------------------------------------------
    OMP_PROC_BIND=TRUE numactl -l ./block4_amgcl_vexcl_cuda   -A tube_3d_simple/A.bin -f tube_3d_simple/b.bin
    OMP_PROC_BIND=TRUE numactl -l ./block4_amgcl_vexcl_opencl -A tube_3d_simple/A.bin -f tube_3d_simple/b.bin
    OMP_PROC_BIND=TRUE numactl -l ./block4_schur_vexcl_cuda   -A tube_3d_simple/A.bin -f tube_3d_simple/b.bin
    OMP_PROC_BIND=TRUE numactl -l ./block4_schur_vexcl_opencl -A tube_3d_simple/A.bin -f tube_3d_simple/b.bin
    OMP_PROC_BIND=TRUE numactl -l ./block4_schur_cuda         -A tube_3d_simple/A.bin -f tube_3d_simple/b.bin
done

# --- CPU runs -----------------------------------------------------------
for n in 1 2 4 8 16; do
    for i in `seq 5`; do
        echo -----------------------------------------------------------------
        echo --- CORES=$n, i=$i
        echo -----------------------------------------------------------------
        OMP_NUM_THREADS=$n OMP_PLACES=sockets numactl -l ./block4_amgcl -A tube_3d_simple/A.bin -f tube_3d_simple/b.bin
        OMP_NUM_THREADS=$n OMP_PLACES=cores   numactl -l ./block4_schur -A tube_3d_simple/A.bin -f tube_3d_simple/b.bin
        OMP_NUM_THREADS=1  mpirun -np $n ./block4_trilinos --matrix=tube_3d_simple/A.bin --RHS=tube_3d_simple/b.bin --part=part-${n}.mtx
        OMP_NUM_THREADS=1  mpirun -np $n ./block4_petsc -A tube_3d_simple/A.bin -f tube_3d_simple/b.bin -p part-${n}.mtx -ksp_monitor
    done
done
